+++
title = 'Oxidizer Rich Staged Combustion Cycle'
date = 2023-10-05T21:32:27-03:00
draft = false
+++

O ciclo de combustão estágio rico em oxidante é um ciclo avançado de propulsão usado em motores de foguetes e sistemas de propulsão espacial. Neste ciclo, o oxidante, geralmente oxigênio líquido (LOX), é usado em excesso em relação ao combustível, criando uma mistura rica em oxidante na câmara de combustão. Antes de serem injetados na câmara de combustão principal, tanto o combustível quanto o oxidante passam por pré-queimadores separados. O combustível é parcialmente queimado em seu pré-queimador, enquanto o oxidante é pré-queimado no seu próprio pré-queimador. Essa abordagem permite um controle preciso da relação de mistura e da temperatura na câmara de combustão principal, resultando em altos níveis de eficiência e desempenho. O ciclo de combustão estágio rico em oxidante é apreciado por sua eficiência e é utilizado em motores de foguetes de alta potência e sistemas de propulsão avançados.