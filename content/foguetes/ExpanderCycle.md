+++
title = 'Expander Cycle'
date = 2023-10-05T21:19:11-03:00
draft = false
+++

O ciclo expansor é um ciclo de propulsão usado em motores de foguetes e outros sistemas de propulsão. Nesse ciclo, um gás criogênico, geralmente hidrogênio líquido (LH2), é usado como propelente. O ciclo envolve o resfriamento desse gás a temperaturas extremamente baixas antes de ser injetado na câmara de combustão. Durante a expansão do gás quente na câmara de combustão, ele se expande e gera energia térmica, que é usada para produzir empuxo. O ciclo expansor é conhecido por sua eficiência e simplicidade em comparação com outros ciclos de propulsão, tornando-o uma escolha popular para veículos espaciais que necessitam de alta eficiência e baixo peso.

