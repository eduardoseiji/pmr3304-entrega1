+++
title = 'Tap-Off Combustion Cycle'
date = 2023-10-05T21:33:00-03:00
draft = false
+++

O ciclo de desvio, ou "tap-off" em inglês, é um ciclo de propulsão utilizado em motores de foguetes e sistemas de propulsão. Nesse ciclo, uma pequena parte dos gases de combustão gerados na câmara de combustão principal é desviada antes de sair pelo bocal de escape. Esses gases desviados são direcionados para alimentar os sistemas de controle do motor, como a direção do empuxo e a estabilidade da trajetória. O ciclo de desvio permite uma maior precisão no controle da direção e no ajuste do impulso do motor, tornando-o adequado para aplicações que requerem manobrabilidade precisa, como foguetes direcionais e espaçonaves. É uma abordagem eficaz para otimizar o desempenho e a estabilidade dos motores de foguetes.