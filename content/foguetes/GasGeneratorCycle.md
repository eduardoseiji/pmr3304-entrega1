+++
title = 'Gas Generator Cycle'
date = 2023-10-05T21:26:25-03:00
draft = false
+++

O ciclo do gerador de gás é um ciclo de propulsão usado em motores de foguetes e outros sistemas de propulsão. Nesse ciclo, um pequeno volume de combustível e oxidante é queimado em uma câmara de pré-combustão separada, conhecida como "gerador de gás". Os gases quentes produzidos nesse processo são usados para acionar uma turbina, que, por sua vez, impulsiona as bombas que fornecem combustível e oxidante para a câmara de combustão principal. Essa separação das correntes de gás quente e propulente principal permite um controle mais preciso da pressão e da temperatura na câmara de combustão, o que é benéfico para ajustar o desempenho do motor. O ciclo do gerador de gás é comumente usado em motores de foguetes e é apreciado por sua flexibilidade e capacidade de regulação.