+++
title = 'Fuel Rich Staged Combustion Cycle'
date = 2023-10-05T21:32:42-03:00
draft = false
+++


O ciclo de combustão estágio rico em combustível é um ciclo avançado de propulsão utilizado em motores de foguetes e sistemas de propulsão espacial. Neste ciclo, o combustível, geralmente hidrogênio líquido (LH2), é usado em excesso em relação ao oxidante, criando uma mistura rica em combustível na câmara de combustão. Tanto o combustível quanto o oxidante são pré-queimados em câmaras de pré-combustão separadas antes de serem injetados na câmara de combustão principal. Essa abordagem permite um controle preciso da relação de mistura e da temperatura na câmara de combustão principal, resultando em altos níveis de eficiência e desempenho. O ciclo de combustão estágio rico em combustível é apreciado por sua eficiência e é utilizado em motores de foguetes de alta potência e sistemas de propulsão avançados.