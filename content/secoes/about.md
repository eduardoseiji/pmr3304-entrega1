+++
title = 'About'
date = 2023-10-05T21:04:52-03:00
draft = false
+++

Este site é de autoria de Eduardo Seiji Suguimoto Miyazato Ferrer, Nº USP 8942638.

Graduando em Engenharia Mecatrônica Poli-USP e graduado em Ciências Moleculares na USP, cujo objetivo é formar pesquisadores com base multi e interdisciplinar nas áreas de Física, Matemática, Computação, Química e Biologia.

Aluno de Iniciação Científica no Laboratório de Combustão e Propulsão do Instituto Nacional de Pesquisas Espaciais (LCP-INPE)

Participo, desde o ensino médio em muitas atividades extracurriculares, como olimpíadas científicas, grupos de extensão, trabalhos voluntários e cursos de capacitação.

Tenho especial interesse em Engenharia Aeroespacial e Automotiva.

Apaixonado por ciência e engenharia e ávido por aprender, encarar novos desafios, resolver problemas e fazer projetos. Sonho em contribuir para a volta do homem à Lua e a ida a Marte.